package com.ciwa_training.Util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class MyJavaScriptInterface {
    private Context ctx;

    public MyJavaScriptInterface(Context ctx) {
        this.ctx = ctx;
    }

    @JavascriptInterface
    public void showHTML(String html) {
        //Document doc = Jsoup.parse(html);
        if (html.contains("222222222")) {
            util.status = "200";
            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent("success"));

        } else if (html.contains("203")) {
            util.status = "203";
            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent("success"));
        }else if (html.contains("444444444")) {
            util.status = "202";
            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent("success"));
        }
        Log.e("URl", html);
        System.out.println(html);
    }
}

