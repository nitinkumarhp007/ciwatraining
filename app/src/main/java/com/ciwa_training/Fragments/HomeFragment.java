package com.ciwa_training.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.Activities.TermConditionActivity;
import com.ciwa_training.Adapters.CategoryHomeAdapter;
import com.ciwa_training.Adapters.HomeAdapter;
import com.ciwa_training.DataModels.MeditationCategories;
import com.ciwa_training.DataModels.PostsModel;
import com.ciwa_training.MainActivity;
import com.ciwa_training.ParserNew.GetMethod;
import com.ciwa_training.ParserNew.Message;
import com.ciwa_training.R;
import com.ciwa_training.Util.ConnectivityReceiver;
import com.ciwa_training.Util.Parameters;
import com.ciwa_training.Util.SavePref;
import com.ciwa_training.Util.util;
import com.ciwa_training.parser.AllAPIS;
import com.ciwa_training.parser.GetAsyncGet;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class HomeFragment extends Fragment {

    Context context;
    Unbinder unbinder;

    @BindView(R.id.my_recycler_view)
    RecyclerView my_recycler_view;
    @BindView(R.id.my_recycler_view_top)
    RecyclerView my_recycler_view_top;
    @BindView(R.id.error_message)
    TextView error_message;

    CategoryHomeAdapter adapter = null;
    HomeAdapter homeAdapter = null;
    ArrayList<MeditationCategories> list;

    ProgressDialog mDialog = null;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();

        mDialog = util.initializeProgress(context);


        if (SavePref.getOpen(context, "open").isEmpty()) {
            //method to show dialog box for consent_to_app_privacy_policy
            show_dialog();
        }


        //method to call api to get all meditation category & posts
        if (ConnectivityReceiver.isConnected())
            MEDITATIONCATEGORYLISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

        return view;


    }

    private void MEDITATIONCATEGORYLISTING() {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.MEDITATIONCATEGORYLISTING, formBody);
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        list = new ArrayList<>();
        if (list.size() > 0)
            list.clear();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonmainObject = new JSONObject(result);
                if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                    JSONArray data = jsonmainObject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        MeditationCategories meditationCategories = new MeditationCategories();
                        meditationCategories.setId(object.getString("id"));
                        meditationCategories.setName(object.getString("name"));
                        meditationCategories.setImage(object.getString("image"));
                        meditationCategories.setDescription(object.getString("description"));
                        if (i == 0)
                            meditationCategories.setIs_selected(true);

                        ArrayList<PostsModel> list_post = new ArrayList<>();
                        for (int j = 0; j < object.getJSONArray("audioCategoryPosts").length(); j++) {
                            JSONObject obj = object.getJSONArray("audioCategoryPosts").getJSONObject(j);
                            PostsModel postsModel = new PostsModel();
                            postsModel.setId(obj.getString("id"));
                            postsModel.setName(obj.getString("name"));
                            postsModel.setImage(obj.getString("image"));
                            postsModel.setDescription(obj.getString("description"));
                            postsModel.setAudio(obj.getString("audio"));
                            postsModel.setType(obj.optString("mediaType"));//mediaType: 0 => audoio, 1 => video
                            list_post.add(postsModel);

                        }
                        if (list_post.size() > 0)
                            Collections.reverse(list_post);
                        meditationCategories.setPost_list(list_post);
                        list.add(meditationCategories);
                    }

                    if (list.size() > 0) {
                        adapter = new CategoryHomeAdapter(context, list, HomeFragment.this);
                        my_recycler_view_top.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        my_recycler_view_top.setAdapter(adapter);

                        if (list.get(0).getPost_list().size() > 0) {
                            homeAdapter = new HomeAdapter(context, list.get(0).getPost_list());
                            //    my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
                            my_recycler_view.setLayoutManager(new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false));
                            my_recycler_view.setAdapter(homeAdapter);

                            my_recycler_view.setVisibility(View.VISIBLE);
                            error_message.setVisibility(View.GONE);
                        } else {
                            error_message.setVisibility(View.VISIBLE);
                            my_recycler_view.setVisibility(View.GONE);
                        }
                    }


                } else {
                    util.IOSDialog(context, jsonmainObject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void Click_Category(int position) {
        list.get(position).setIs_selected(true);
        adapter.notifyDataSetChanged();

        if (list.get(position).getPost_list().size() > 0) {

            homeAdapter = new HomeAdapter(context, list.get(position).getPost_list());
            my_recycler_view.setLayoutManager(new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false));
            my_recycler_view.setAdapter(homeAdapter);

            my_recycler_view.setVisibility(View.VISIBLE);
            error_message.setVisibility(View.GONE);

        } else {

            error_message.setVisibility(View.VISIBLE);
            my_recycler_view.setVisibility(View.GONE);

        }

    }


    private void show_dialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView term_check = (TextView) dialog.findViewById(R.id.term_check);
        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView no = (TextView) dialog.findViewById(R.id.no);
        term_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent11111 = new Intent(context, TermConditionActivity.class);
                intent11111.putExtra("type", "privacy");
                startActivity(intent11111);
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SavePref.setOpen(context, "open", "1");
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.context.finish();
            }
        });

        dialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}