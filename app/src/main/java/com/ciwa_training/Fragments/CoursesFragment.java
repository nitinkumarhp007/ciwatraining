package com.ciwa_training.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.Adapters.CourseAdapter;
import com.ciwa_training.DataModels.PostsModel;
import com.ciwa_training.ParserNew.GetMethod;
import com.ciwa_training.ParserNew.Message;
import com.ciwa_training.ParserNew.PostMethod;
import com.ciwa_training.R;
import com.ciwa_training.Util.ConnectivityReceiver;
import com.ciwa_training.Util.Parameters;
import com.ciwa_training.Util.util;
import com.ciwa_training.parser.AllAPIS;
import com.ciwa_training.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class CoursesFragment extends Fragment {

    Context context;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    ArrayList<PostsModel> list;
    ProgressDialog mDialog;

    public CoursesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_courses, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        mDialog = util.initializeProgress(context);


        //method to call api to get all practical page data
        if (ConnectivityReceiver.isConnected())
            PRACTICELISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

        return view;
    }

    private void PRACTICELISTING() {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();

        GetMethod getAsyncNew = new GetMethod(context, AllAPIS.PRACTICELISTING, formBody);
        getAsyncNew.hitApi();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        list = new ArrayList<>();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonmainObject = new JSONObject(result);
                if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                    JSONArray data = jsonmainObject.getJSONArray("body");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        PostsModel postsModel = new PostsModel();
                        postsModel.setId(object.getString("id"));
                        postsModel.setName(object.getString("name"));
                        postsModel.setImage(object.getString("image"));
                        postsModel.setDescription(object.getString("description"));
                        postsModel.setDescription(object.getString("description"));
                        postsModel.setAudio(object.getString("audio"));
                        postsModel.setType(object.optString("mediaType"));//mediaType: 0 => audoio, 1 => video
                        list.add(postsModel);
                    }
                    myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false));
                    myRecyclerView.setAdapter(new CourseAdapter(context,list));

                } else {
                    util.IOSDialog(context, jsonmainObject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}