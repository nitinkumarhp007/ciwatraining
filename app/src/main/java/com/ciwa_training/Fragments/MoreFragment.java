package com.ciwa_training.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.ciwa_training.Activities.ContactUsActivity;
import com.ciwa_training.Activities.TermConditionActivity;
import com.ciwa_training.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MoreFragment extends Fragment {


    Context context;
    Unbinder unbinder;
    @BindView(R.id.contact_us_text)
    Button contactUsText;
    @BindView(R.id.contact_us)
    RelativeLayout contactUs;
    @BindView(R.id.about_this_app_text)
    Button aboutThisAppText;
    @BindView(R.id.about_this_app)
    RelativeLayout aboutThisApp;
    @BindView(R.id.disclaimer_text)
    Button disclaimerText;
    @BindView(R.id.disclaimer)
    RelativeLayout disclaimer;
    @BindView(R.id.privacy_policy_text)
    Button privacyPolicyText;
    @BindView(R.id.privacy_policy)
    RelativeLayout privacyPolicy;

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.contact_us_text, R.id.contact_us, R.id.about_this_app_text, R.id.about_this_app, R.id.disclaimer_text, R.id.disclaimer, R.id.privacy_policy_text, R.id.privacy_policy})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_us_text:
                //method to call ContactUs page
                startActivity(new Intent(context, ContactUsActivity.class));
                break;
            case R.id.contact_us:
                //method to call ContactUs page
                startActivity(new Intent(context, ContactUsActivity.class));
                break;
            case R.id.about_this_app_text:
                //method to call about US page
                Intent intent112 = new Intent(context, TermConditionActivity.class);
                intent112.putExtra("type", "about");
                startActivity(intent112);
                break;
            case R.id.about_this_app:
                //method to call about US page
                Intent intent1144 = new Intent(context, TermConditionActivity.class);
                intent1144.putExtra("type", "about");
                startActivity(intent1144);
                break;

            case R.id.disclaimer:
                //method to call disclaimer page
                Intent intent11 = new Intent(context, TermConditionActivity.class);
                intent11.putExtra("type", "disclaimer");
                startActivity(intent11);
                break;
            case R.id.disclaimer_text:
                //method to call disclaimer page
                Intent intent111 = new Intent(context, TermConditionActivity.class);
                intent111.putExtra("type", "disclaimer");
                startActivity(intent111);
                break;
            case R.id.privacy_policy:
                //method to call privacy page
                Intent intent11111 = new Intent(context, TermConditionActivity.class);
                intent11111.putExtra("type", "privacy");
                startActivity(intent11111);
                break;
            case R.id.privacy_policy_text:
                //method to call privacy page
                Intent intent1112 = new Intent(context, TermConditionActivity.class);
                intent1112.putExtra("type", "privacy");
                startActivity(intent1112);
                break;
        }
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}