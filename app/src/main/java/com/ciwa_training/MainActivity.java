package com.ciwa_training;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ciwa_training.Fragments.CoursesFragment;
import com.ciwa_training.Fragments.EducationFragment;
import com.ciwa_training.Fragments.HomeFragment;
import com.ciwa_training.Fragments.MoreFragment;
import com.ciwa_training.Util.SavePref;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.title)
    TextView title;
    private SavePref savePref;
    public Toolbar toolbar;
    public static MainActivity context;
    public BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = MainActivity.this;

        savePref = new SavePref(context);

        Log.e("auth_key", savePref.getAuthorization_key());
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.setSelectedItemId(R.id.navigation_home);

        Log.e("token____", SavePref.getDeviceToken(this, "token"));

    }

    //method to set bottom tabs on main screen
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            toolbar.setVisibility(View.VISIBLE);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    title.setText("Home");
                    loadFragment(new HomeFragment());
                    return true;
                case R.id.navigation_courses:
                    //toolbar.setVisibility(View.GONE);
                    loadFragment(new CoursesFragment());
                    title.setText("Exercises");
                    return true;
                case R.id.navigation_education:
                    // toolbar.setVisibility(View.GONE);
                    loadFragment(new EducationFragment());
                    title.setText("Evaluation");
                    return true;
                case R.id.navigation_about:
                    // toolbar.setVisibility(View.GONE);
                    loadFragment(new MoreFragment());
                    title.setText("Account");
                    return true;
            }
            return false;
        }
    };

    //method to load page like Learn, Practice, Assessment , More
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }
}
