package com.ciwa_training.DataModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class MeditationCategories implements Parcelable {
    String id="";
    String name="";
    String description="";
    String image="";
    String num_of_questions="";
    boolean is_selected=false;
    ArrayList<PostsModel> post_list;

    public MeditationCategories()
    {}

    protected MeditationCategories(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        image = in.readString();
        num_of_questions = in.readString();
        is_selected = in.readByte() != 0;
        post_list = in.createTypedArrayList(PostsModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(num_of_questions);
        dest.writeByte((byte) (is_selected ? 1 : 0));
        dest.writeTypedList(post_list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MeditationCategories> CREATOR = new Creator<MeditationCategories>() {
        @Override
        public MeditationCategories createFromParcel(Parcel in) {
            return new MeditationCategories(in);
        }

        @Override
        public MeditationCategories[] newArray(int size) {
            return new MeditationCategories[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNum_of_questions() {
        return num_of_questions;
    }

    public void setNum_of_questions(String num_of_questions) {
        this.num_of_questions = num_of_questions;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }

    public ArrayList<PostsModel> getPost_list() {
        return post_list;
    }

    public void setPost_list(ArrayList<PostsModel> post_list) {
        this.post_list = post_list;
    }
}
