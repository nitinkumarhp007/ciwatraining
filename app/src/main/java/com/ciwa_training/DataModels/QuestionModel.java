package com.ciwa_training.DataModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class QuestionModel implements Parcelable {
    String id="";
    String question="";
    boolean is_question_answered=false;
    ArrayList<AnswerModel> list;

    public QuestionModel()
    {}

    protected QuestionModel(Parcel in) {
        id = in.readString();
        question = in.readString();
        is_question_answered = in.readByte() != 0;
        list = in.createTypedArrayList(AnswerModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(question);
        dest.writeByte((byte) (is_question_answered ? 1 : 0));
        dest.writeTypedList(list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionModel> CREATOR = new Creator<QuestionModel>() {
        @Override
        public QuestionModel createFromParcel(Parcel in) {
            return new QuestionModel(in);
        }

        @Override
        public QuestionModel[] newArray(int size) {
            return new QuestionModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isIs_question_answered() {
        return is_question_answered;
    }

    public void setIs_question_answered(boolean is_question_answered) {
        this.is_question_answered = is_question_answered;
    }

    public ArrayList<AnswerModel> getList() {
        return list;
    }

    public void setList(ArrayList<AnswerModel> list) {
        this.list = list;
    }
}
