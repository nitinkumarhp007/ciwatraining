package com.ciwa_training.DataModels;

import android.os.Parcel;
import android.os.Parcelable;

public class AnswerModel implements Parcelable {
    String id="";
    String optionText="";
    String score="";
    boolean is_selected=false;

    public AnswerModel()
    {}

    protected AnswerModel(Parcel in) {
        id = in.readString();
        optionText = in.readString();
        score = in.readString();
        is_selected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(optionText);
        dest.writeString(score);
        dest.writeByte((byte) (is_selected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AnswerModel> CREATOR = new Creator<AnswerModel>() {
        @Override
        public AnswerModel createFromParcel(Parcel in) {
            return new AnswerModel(in);
        }

        @Override
        public AnswerModel[] newArray(int size) {
            return new AnswerModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }
}
