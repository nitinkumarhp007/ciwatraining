package com.ciwa_training.parser;


public class AllAPIS {


    //public static final String BASE_URL = "http://3.6.100.60:9222/api/";
    public static final String BASE_URL = "http://54.82.244.85:7200/api/";


    public static final String MEDITATIONCATEGORYLISTING = BASE_URL + "meditationCategoriesListing";
    public static final String QUESTIONCATEGORYLISTING = BASE_URL + "questionCategoriesListing";
    public static final String QUESTIONCATEGORYQUESTIONLISTING = BASE_URL + "questionCategoryQuestionsListing";
    public static final String PRACTICELISTING = BASE_URL + "practiceListing";
    public static final String CONTACTUS = BASE_URL + "contactUs";
    public static final String SUBMITANSWER = BASE_URL + "submitAnswers";
    public static final String ABOUTUS = BASE_URL + "aboutUs";
    public static final String TERMSANDCONDITIONS = BASE_URL + "termsAndConditions";
    public static final String PRIVACYPOLICY = BASE_URL + "privacyPolicy";
    public static final String PAGES = BASE_URL + "pages";

}
