package com.ciwa_training.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.DataModels.QuestionModel;
import com.ciwa_training.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<QuestionModel> list;

    public EducationAdapter(Context context, ArrayList<QuestionModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.education_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getQuestion());

        holder.myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.myRecyclerView.setAdapter(new AnswerAdapter(context,list,position,list.get(position).getList()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.my_recycler_view)
        RecyclerView myRecyclerView;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
