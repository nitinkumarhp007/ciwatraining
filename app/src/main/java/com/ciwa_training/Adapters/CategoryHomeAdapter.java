package com.ciwa_training.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.Activities.ViewAllActivity;
import com.ciwa_training.DataModels.MeditationCategories;
import com.ciwa_training.Fragments.HomeFragment;
import com.ciwa_training.MainActivity;
import com.ciwa_training.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CategoryHomeAdapter extends RecyclerView.Adapter<CategoryHomeAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    HomeFragment homeFragment;
    private View view;
    ArrayList<MeditationCategories> list;

    public CategoryHomeAdapter(Context context, ArrayList<MeditationCategories> list, HomeFragment homeFragment) {
        this.context = context;
        this.list = list;
        this.homeFragment = homeFragment;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.category_home_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.categoryTitle.setText(list.get(position).getName());

        if (list.get(position).isIs_selected()) {
            holder.categoryTitle.setBackground(context.getResources().getDrawable(R.drawable.tab));
        } else {
            holder.categoryTitle.setBackground(context.getResources().getDrawable(R.drawable.tab1));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setIs_selected(false);
                }
                homeFragment.Click_Category(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.category_title)
        TextView categoryTitle;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
