package com.ciwa_training.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.Activities.StartTestActivity;
import com.ciwa_training.DataModels.MeditationCategories;
import com.ciwa_training.MainActivity;
import com.ciwa_training.R;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TestSubjectAdapter extends RecyclerView.Adapter<TestSubjectAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<MeditationCategories> list;

    public TestSubjectAdapter(Context context, ArrayList<MeditationCategories> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.test_sub_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context).load(list.get(position).getImage()).into(holder.image);
        holder.name.setText(list.get(position).getName());
        holder.numOfQuestions.setText(list.get(position).getNum_of_questions() + " Questions");


        //method to start test
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StartTestActivity.class);
                intent.putExtra("questionCategoryId", list.get(position).getId());
                intent.putExtra("name", list.get(position).getName());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.num_of_questions)
        TextView numOfQuestions;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
