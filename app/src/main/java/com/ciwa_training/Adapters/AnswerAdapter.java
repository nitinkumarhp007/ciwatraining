package com.ciwa_training.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.DataModels.AnswerModel;
import com.ciwa_training.DataModels.QuestionModel;
import com.ciwa_training.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<AnswerModel> list;
    ArrayList<QuestionModel> question_list;
    int pos = 0;

    public AnswerAdapter(Context context, ArrayList<QuestionModel> question_list, int pos, ArrayList<AnswerModel> list) {
        this.context = context;
        this.list = list;
        this.pos = pos;
        this.question_list = question_list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.answer_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (list.get(position).isIs_selected())
            holder.answerRadio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.redio_c, 0, 0, 0);
        else
            holder.answerRadio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.redio_e, 0, 0, 0);

        holder.answerRadio.setText(list.get(position).getOptionText());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).isIs_selected())
                        list.get(i).setIs_selected(false);
                }
                list.get(position).setIs_selected(true);
                question_list.get(pos).setIs_question_answered(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.answer_radio)
        TextView answerRadio;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
