package com.ciwa_training.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.Adapters.EducationAdapter;
import com.ciwa_training.DataModels.AnswerModel;
import com.ciwa_training.DataModels.QuestionModel;
import com.ciwa_training.ParserNew.Message;
import com.ciwa_training.ParserNew.PostMethod;
import com.ciwa_training.R;
import com.ciwa_training.Util.ConnectivityReceiver;
import com.ciwa_training.Util.Parameters;
import com.ciwa_training.Util.SavePref;
import com.ciwa_training.Util.util;
import com.ciwa_training.parser.AllAPIS;
import com.ciwa_training.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class StartTestActivity extends AppCompatActivity {
    StartTestActivity context;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.submit)
    Button submit;
    String questionCategoryId = "";
    ProgressDialog mDialog = null;
    ArrayList<QuestionModel> list;

    boolean is_list = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_test);
        ButterKnife.bind(this);
        setToolbar();

        context = StartTestActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        questionCategoryId = getIntent().getStringExtra("questionCategoryId");


        //method to call api to get all questions
        if (ConnectivityReceiver.isConnected())
            QUESTIONCATEGORYQUESTIONLISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void QUESTIONCATEGORYQUESTIONLISTING() {
        is_list = true;
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.QUESTIONCATEGORYID, questionCategoryId);
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.QUESTIONCATEGORYQUESTIONLISTING, formBody, "");
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        String result = event.getMessage();
        mDialog.dismiss();
        if (is_list) {
            //questionCategoryQuestionsListing api responce
            list = new ArrayList<>();
            if (result != null && !result.equalsIgnoreCase("")) {
                try {
                    JSONObject jsonmainObject = new JSONObject(result);
                    if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                        JSONArray data = jsonmainObject.getJSONArray("body");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            QuestionModel questionModel = new QuestionModel();
                            questionModel.setId(object.getString("id"));
                            questionModel.setIs_question_answered(false);
                            questionModel.setQuestion(object.getString("question"));

                            ArrayList<AnswerModel> list_answer = new ArrayList<>();
                            for (int j = 0; j < object.getJSONArray("categoryQuestionOptions").length(); j++) {
                                JSONObject obj = object.getJSONArray("categoryQuestionOptions").getJSONObject(j);
                                AnswerModel answerModel = new AnswerModel();
                                answerModel.setId(obj.getString("id"));
                                answerModel.setOptionText(obj.getString("optionText"));
                                answerModel.setScore(obj.getString("score"));
                                answerModel.setIs_selected(false);
                                list_answer.add(answerModel);

                            }
                            questionModel.setList(list_answer);
                            list.add(questionModel);
                        }
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                        myRecyclerView.setAdapter(new EducationAdapter(context,list));

                    } else {
                        util.IOSDialog(context, jsonmainObject.getString("message"));
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            //submit answer api responce
            if (result != null && !result.equalsIgnoreCase("")) {
                try {
                    JSONObject jsonMainobject = new JSONObject(result);
                    if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                        JSONObject body = jsonMainobject.getJSONObject("body");
                        show_dialog(body.getString("score"), body.getString("ranking"));
                    } else {
                        util.IOSDialog(context, jsonMainobject.getString("message"));
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


    //method to set top bar
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getIntent().getStringExtra("name"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            //method to go back to previews screen
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.submit)
    public void onClick() {
        String optionIds = "";
        boolean answer_all = true;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isIs_question_answered()) {
                answer_all = true;
            } else {
                answer_all = false;
            }
            for (int j = 0; j < list.get(i).getList().size(); j++) {
                if (list.get(i).getList().get(j).isIs_selected()) {
                    if (optionIds.isEmpty())
                        optionIds = list.get(i).getList().get(j).getId();
                    else
                        optionIds = optionIds + "," + list.get(i).getList().get(j).getId();
                }
            }
        }
        if (ConnectivityReceiver.isConnected()) {
            if (answer_all)
                SUBMITANSWER_API(optionIds);
            else
                util.IOSDialog(context, "Please answer all questions.");
        } else
            util.IOSDialog(context, util.internet_Connection_Error);

    }


    private void SUBMITANSWER_API(String optionIds) {
        Log.e("optionIdsss", optionIds);
        is_list=false;
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, questionCategoryId);
        formBuilder.addFormDataPart(Parameters.OPTIONIDS, optionIds);
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.SUBMITANSWER, formBody, "");
        getAsyncNew.hitApi();
    }

    private void show_dialog(String score_text, String ranking_text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_12);

        TextView score = (TextView) dialog.findViewById(R.id.score);
        TextView ranking = (TextView) dialog.findViewById(R.id.ranking);

        score.setText("Your Score: " + score_text);
        // ranking.setText("Your Ranking: " + ranking_text);
        ranking.setText(ranking_text);

        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //method to go back to previews screen
                finish();
            }
        });

        dialog.show();
    }

}