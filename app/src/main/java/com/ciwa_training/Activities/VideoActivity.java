package com.ciwa_training.Activities;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.ciwa_training.R;
import com.ciwa_training.Util.util;

public class VideoActivity extends AppCompatActivity {

    boolean is_portrait = true;

    MediaController mediaController;
    VideoView simpleVideoView;
    ProgressDialog progressDialog;
    MediaPlayer mediaPlayer;
    int stopPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideSystemUI(getWindow());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);


        progressDialog = util.initializeProgress(this);
        progressDialog.show();

        Uri uri = Uri.parse(getIntent().getStringExtra("video_url"));
        simpleVideoView = (VideoView) findViewById(R.id.play_video); // initiate a video view
        ImageView orientation = (ImageView) findViewById(R.id.orientation); // initiate a video view
        simpleVideoView.setVideoURI(uri);


        simpleVideoView.start();


        simpleVideoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {

                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    // Here the video starts
                    progressDialog.dismiss();
                    return true;
                }
                return false;
            }
        });



      /*  mediaController = new MediaController(this);
        mediaController.setAnchorView(simpleVideoView);
        simpleVideoView.setMediaController(mediaController);*/

        //method to set orientation like horizonal or veritical
        orientation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (is_portrait) {
                    is_portrait = false;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    is_portrait = true;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }


            }
        });

        simpleVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer = mp;
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        /*
                         *  add media controller
                         */
                        mediaController = new MediaController(VideoActivity.this);
                        ;
                        simpleVideoView.setMediaController(mediaController);
                        /*
                         * and set its position on screen
                         */
                        mediaController.setAnchorView(simpleVideoView);
                    }
                });
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("dataaa", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("dataaa", "onPause");

       /* if (mediaPlayer != null)
            mediaPlayer.pause();
*/
        stopPosition = simpleVideoView.getCurrentPosition(); //stopPosition is an int
        simpleVideoView.pause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("dataaa", "onDeonResumestroy");

       /* if (mediaPlayer != null)
            mediaPlayer.start();*/

        simpleVideoView.start();
        simpleVideoView.seekTo(stopPosition);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        hideSystemUI(getWindow());
    }
    //method to hide the top bar & navigation keys
    public void hideSystemUI(Window window) {
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
    }
}