package com.ciwa_training.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ciwa_training.ParserNew.GetMethod;
import com.ciwa_training.ParserNew.Message;
import com.ciwa_training.ParserNew.PostMethod;
import com.ciwa_training.R;
import com.ciwa_training.Util.util;
import com.ciwa_training.parser.AllAPIS;
import com.ciwa_training.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class TermConditionActivity extends AppCompatActivity {

    TermConditionActivity context;
    @BindView(R.id.term_text)
    TextView termText;
    ProgressDialog mDialog = null;
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);
        type = getIntent().getStringExtra("type");
        ButterKnife.bind(this);


        //method to set top bar
        setToolbar();

        context = TermConditionActivity.this;
        mDialog = util.initializeProgress(this);
        //method to call api to get all info
        APP_INFO_API();
    }

    private void APP_INFO_API() {
        String url = "";
        if (type.equals("disclaimer")) {
            url = AllAPIS.TERMSANDCONDITIONS;
        } else if (type.equals("about")) {
            url = AllAPIS.ABOUTUS;
        } else {
            url = AllAPIS.PRIVACYPOLICY;
        }
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        GetMethod getAsyncNew = new GetMethod(context, url, formBody);
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                    termText.setText(Html.fromHtml(jsonObject.getJSONObject("body").getString("content")));
                } else {
                    util.showToast(context, jsonObject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (type.equals("privacy"))
            title.setText("Privacy Policy");
        else if (type.equals("disclaimer"))
            title.setText("Disclaimer");
        else
            title.setText("About this app");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            //method to go back to previews screen
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
