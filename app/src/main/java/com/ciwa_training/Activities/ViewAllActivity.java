package com.ciwa_training.Activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciwa_training.Adapters.ViewAllAdapter;
import com.ciwa_training.DataModels.MeditationCategories;
import com.ciwa_training.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewAllActivity extends AppCompatActivity {
    ViewAllActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    MeditationCategories meditationCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all);
        ButterKnife.bind(this);

        context = ViewAllActivity.this;
        meditationCategories = getIntent().getExtras().getParcelable("data");

        //method to set top bar
        setToolbar();

        //method to view of all posts
        myRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        myRecyclerView.setAdapter(new ViewAllAdapter(context, meditationCategories.getPost_list()));

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(meditationCategories.getName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            //method to go back to previews screen
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}