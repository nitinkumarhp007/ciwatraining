package com.ciwa_training.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ciwa_training.DataModels.PostsModel;
import com.ciwa_training.R;
import com.ciwa_training.Util.SavePref;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.vincan.medialoader.MediaLoader;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailActivity extends AppCompatActivity implements View.OnTouchListener, MediaPlayer.OnCompletionListener,
        MediaPlayer.OnBufferingUpdateListener {
    DetailActivity context;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.title_name)
    TextView title_name;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.seekbar_audio)
    SeekBar seekBar;
    @BindView(R.id.backwrad)
    ImageView backwrad;
    @BindView(R.id.play)
    ImageView play;
    @BindView(R.id.forward)
    ImageView forward;
    @BindView(R.id.currentDuration)
    TextView currentDuration;
    @BindView(R.id.video_layout)
    LinearLayout video_layout;
    @BindView(R.id.image_video)
    ImageView image_video;
    @BindView(R.id.title_name__video)
    TextView title_name__video;
    @BindView(R.id.description__video)
    TextView description__video;
    @BindView(R.id.play_video)
    ImageView play_video;
    @BindView(R.id.audio_layout)
    LinearLayout audio_layout;

    private SavePref savePref;

    PostsModel postsModel;

    private MediaPlayer mediaPlayer;
    private int lengthOfAudio;

    private boolean is_play = false;

    private int seek_progress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        //method to set top bar
        setToolbar();

        context = DetailActivity.this;
        savePref = new SavePref(context);


        setdata();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MediaLoader.getInstance(this).destroy();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }

    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Detail");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            //method to go back to previews screen
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.backwrad, R.id.play, R.id.forward})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backwrad:
                if (seek_progress > 3)
                    mediaPlayer.seekTo((seek_progress - 3) * 1000);
                break;
            case R.id.play:
                if (!is_play) {
                    is_play = true;
                    play.setImageDrawable(getResources().getDrawable(R.drawable.pp));

                    try {
                        if (mediaPlayer == null) {
                            mediaPlayer = new MediaPlayer();
                        }
                        mediaPlayer.setDataSource(postsModel.getAudio());
                        mediaPlayer.prepare();
                        lengthOfAudio = mediaPlayer.getDuration();

                        int durationn = mediaPlayer.getDuration();
                        String time = String.format("%02d:%02d",
                                TimeUnit.MILLISECONDS.toMinutes(durationn),
                                TimeUnit.MILLISECONDS.toSeconds(durationn) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationn))
                        );
                        Log.e("time_song:", String.valueOf(durationn) + "  " + time);
                        currentDuration.setText(time);
                        currentDuration.setVisibility(View.VISIBLE);


                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());
                    }

                    seekBar.setMax(lengthOfAudio / 1000);
                    playAudio();
                } else {
                    is_play = false;
                    play.setImageDrawable(getResources().getDrawable(R.drawable.p));
                    try {
                        //mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDataSource(postsModel.getAudio());
                        mediaPlayer.prepare();
                        lengthOfAudio = mediaPlayer.getDuration();

                        seekBar.setMax(lengthOfAudio / 1000);

                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());
                    }
                    pauseAudio();
                }
                break;
            case R.id.forward:
                if (seek_progress < mediaPlayer.getDuration())
                    mediaPlayer.seekTo((seek_progress + 3) * 1000);
                break;
        }
    }

    //method to stop audio
    private void stopAudio() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        //btn_play.setEnabled(true);
        //btn_pause.setEnabled(false);
        //btn_stop.setEnabled(false);
        //seekBar.setProgress(0);
    }

    //method to pause audio
    private void pauseAudio() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
        // btn_play.setEnabled(true);
        //btn_pause.setEnabled(false);
    }

    //method to play audio
    private void playAudio() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }

        final Handler mHandler = new Handler();
//Make sure you update Seekbar on UI thread
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int mCurrentPosition = mediaPlayer.getCurrentPosition() / 1000;
                    seekBar.setProgress(mCurrentPosition);
                }
                mHandler.postDelayed(this, 1000);
            }
        });
        // btn_play.setEnabled(false);
        // btn_pause.setEnabled(true);
        // btn_stop.setEnabled(true);
    }


   /* private void updateSeekProgress() {
        if (mediaPlayer.isPlaying()) {
            seekBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / lengthOfAudio) * 100));
            handler.postDelayed(r, 500);
        }
    }

    private final Handler handler = new Handler();
    private final Runnable r = new Runnable() {
        @Override
        public void run() {
            updateSeekProgress();
        }
    };*/

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int percent) {
        seekBar.setSecondaryProgress(percent);
    }


    @Override
    public void onCompletion(MediaPlayer mp) {
        is_play = false;
        play.setImageDrawable(getResources().getDrawable(R.drawable.p));
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(postsModel.getAudio());
            mediaPlayer.prepare();
            lengthOfAudio = mediaPlayer.getDuration();

        } catch (Exception e) {
            //Log.e("Error", e.getMessage());
        }
        stopAudio();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (mediaPlayer.isPlaying()) {
            seekBar = (SeekBar) v;
            mediaPlayer.seekTo((lengthOfAudio / 100) * seekBar.getProgress());
            Log.e("seek_progress", String.valueOf((lengthOfAudio / 100) * seekBar.getProgress()));

            //    mediaPlayer.seekTo((seek_progress - 3) * 1000);
        }
        return false;
    }

    private void setdata() {

        postsModel = getIntent().getExtras().getParcelable("data");   //get data from model

        if (postsModel.getType().equals("1")) {
            audio_layout.setVisibility(View.GONE);
            video_layout.setVisibility(View.VISIBLE);

            Glide.with(context).load(postsModel.getImage()).into(image_video);
            title_name__video.setText(postsModel.getName());
            description__video.setText(postsModel.getDescription());

        } else {
            audio_layout.setVisibility(View.VISIBLE);
            video_layout.setVisibility(View.GONE);

            Glide.with(context).load(postsModel.getImage()).into(image);
            title_name.setText(postsModel.getName());
            description.setText(postsModel.getDescription());

            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnCompletionListener(this);


            //method to check how progress changes of audio play (start/stop)
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mediaPlayer != null)
                        mediaPlayer.seekTo(seekBar.getProgress() * 1000);
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (mediaPlayer != null && !fromUser) {
                        // mediaPlayer.seekTo(progress * 1000);

                        seek_progress = progress;

                        Log.e("seek_progress", String.valueOf(progress));

                        int durationn = seek_progress * 1000;
                        String time = String.format("%02d:%02d",
                                TimeUnit.MILLISECONDS.toMinutes(durationn),
                                TimeUnit.MILLISECONDS.toSeconds(durationn) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationn))
                        );
                        Log.e("time_song:", String.valueOf(durationn) + "  " + time);
                        currentDuration.setText(time);
                        currentDuration.setVisibility(View.VISIBLE);
                    }


                }
            });

        }

        //method to play videos
        play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!postsModel.getAudio().isEmpty() && postsModel.getType().equals("1")) {
                    /*context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                            postsModel.getAudio(),
                            " ", 0));*/
                    Intent intent = new Intent(context, VideoActivity.class);
                    intent.putExtra("video_url", postsModel.getAudio());
                    startActivity(intent);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
            }
        });

    }


}