package com.ciwa_training.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ciwa_training.ParserNew.Message;
import com.ciwa_training.ParserNew.PostMethod;
import com.ciwa_training.R;
import com.ciwa_training.Util.ConnectivityReceiver;
import com.ciwa_training.Util.Parameters;
import com.ciwa_training.Util.SavePref;
import com.ciwa_training.Util.util;
import com.ciwa_training.parser.AllAPIS;
import com.ciwa_training.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ContactUsActivity extends AppCompatActivity {

    ContactUsActivity context;
    SavePref savePref;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.change)
    Button change;

    ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        context = ContactUsActivity.this;
        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        setToolbar();

    }

    //method to set top bar of page
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Contact Us");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            //method to go back to previews screen
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.change)
    public void onClick() {
        if (ConnectivityReceiver.isConnected()) {
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (email.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(email.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } /*else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } */ else if (description.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Description");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                util.hideKeyboard(context);
                //method to call CONTACT_US_API
                CONTACT_US_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void CONTACT_US_API() {
        util.hideKeyboard(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        PostMethod getAsyncNew = new PostMethod(context, AllAPIS.CONTACTUS, formBody, "");
        getAsyncNew.hitApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event) {
        mDialog.dismiss();
        String result = event.getMessage();
        if (result != null && !result.equalsIgnoreCase("")) {
            try {
                JSONObject jsonMainobject = new JSONObject(result);
                if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage("Sent successfully to admin").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //method to go back to previews screen
                            finish();
                        }
                    })
                            /* .setNegativeButton("Cancel", null)*/.show();


                } else {
                    util.IOSDialog(context, jsonMainobject.getString("message"));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}

